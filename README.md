# Discord Bot template

This is a Discord Bot template made to speed up the setup process of future bots.

## Setup

- Run `npm install` to install the required dependencies.
- Rename `.env.example` to `.env` and complete it with your configuration values.

## NPM Scripts

- `start`: Runs the bot with `node`.
- `dev`: Runs the bot with `nodemon` to reload when saving changes.
- `register-app-commands`: Register/update application commands for the bot.

## Packages used

- [Discord.js](https://discord.js.org)
- [Eslint](https://eslint.org)
- [Prettier](https://prettier.io)

## Contact

You can find me on [Discord](https://discord.com) as `Andresto#0350`.
